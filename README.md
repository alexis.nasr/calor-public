# The CALOR-Frame Corpus

Semantic Frames annotated corpus realized by Orange and Aix Marseille University



## Contents

* **[conllu](conllu)** the new version of the CALOR corpus: correct tokenisation with Frames and Coreferences (LIS coreferences)
* **[html](html)** CALOR as HTML (for better readability)

### CoNLL-U format

Calor is annotated using the [CoNLL-U format](https://universaldependencies.org/format.html) used by Universal Dependencies. Columns 11 to 13 are our extension in order to add our semantic annotations


1. ID: Word index, integer starting at 1 for each new sentence; may be a range for multiword tokens; may be a decimal number for empty nodes (decimal numbers can be lower than 1 but must be greater than 0).
2. FORM: Word form or punctuation symbol.
3. LEMMA: Lemma or stem of word form.
4. UPOS: Universal part-of-speech tag.
5. XPOS: Language-specific part-of-speech tag; underscore if not available.
6. FEATS: List of morphological features from the universal feature inventory or from a defined language-specific extension (list of key=value fields, seperated by `|`); underscore if not available.
7. HEAD: Head of the current word, which is either a value of ID or zero (0).
8. DEPREL: Universal dependency relation to the HEAD (root iff HEAD = 0) or a defined language-specific subtype of one.
9. DEPS: Enhanced dependency graph in the form of a list of head-deprel pairs.
10. MISC: Any other annotation (list of key=value fields, seperated by `|`)
11. Named entity type/reference
12. Frame semantics (Targets, Frame elements; list of BIO elements seperated by `|`)
13. Coreferences (for Frame elements only; list of BIO elements seperated by `|`)

### Example

If a word has more than one Frame or coreference annotated, they must be in the same column, separated by a vertical bar 
(for better readability the column with morphological features has been emptied)

```
ID   FORM      LEMMA    UPOS    XPOS    FEATS HEAD DEPREL  DEPS MISC          NE FRAMES                                                  COREF
1    En        en       ADP     ADP     _     3    case    _                  _  B:Colonization:FE:Time:G:465|B:Buildings:FE:Time:G:472  _
2    -         -        PUNCT   PUNCT   _     3    punct   _                  _  I:Colonization:FE:Time:G:465|I:Buildings:FE:Time:G:472  _
3    546       546      NUM     NUM     _     0    root    _    SpaceAfter=No _  I:Colonization:FE:Time:G:465|I:Buildings:FE:Time:G:472  _
4    ,         ,        PUNCT   PUNCT   _     3    punct   _    _             _  _                                                       _
5    les       le       DET     ART     _     6    det     _    _             _  _                                                       B:coref21:T65:CommonNoun
6    habitants habitant NOUN    NOUN    _     3    appos   _    _             _  _                                                       I:coref21:T65:CommonNoun
7    de        de       ADP     ADP     _     8    case    _    _             _  _                                                       I:coref21:T65:CommonNoun
8    Phocée    Phocée   PROPN   PROPN   _     6    nmod    _    SpaceAfter=No _  _                                                       B:coref23:T74:Place|I:coref21:T65:CommonNoun
9    ,         ,        PUNCT   PUNCT   _     6    punct   _    _             _  _                                                       _
10   chassés   chasser  VERB    PARTP   _     6    acl     _    _             _  _                                                       _
11   de        de       ADP     ADP     _    13    case    _    _             _  _                                                       _
12   leur      son      DET     DET     _    13    det     _    _             _  _                                                       B:coref21:T72:Possessive|B:coref23:T75:CommonNoun
13   cité      cité     NOUN    NOUN    _    10    obl:arg _    _             _  _                                                       I:coref23:T75:CommonNoun
14   ionienne  ionien   ADJ     ADJ     _    13    amod    _    SpaceAfter=No _  _                                                       I:coref23:T75:CommonNoun
```

### Contributors

* Jeremy Auguste (AMU)
* Frederic Bechet (AMU)
* Delphine Charlet (Orange)
* Geraldine Damnati (Orange)
* Marianne Djemaa (AMU)
* Laure Dupont (AMU)
* Benoit Favre (AMU)
* Camille Gobert (AMU)
* Johannes Heinecke (Orange)
* Frederic Herledan (Orange)
* Gabriel Marzinotto (AMU/Orange)
* Alexis Nasr (AMU)
* Manon Scholivet (AMU)
* Matthieu Stali (AMU)

### Reference

To cite the CALOR corpus please cite :

```
@inproceedings{marzinotto-etal-2018-semantic,
    title = "Semantic Frame Parsing for Information Extraction : the {CALOR} corpus",
    author = "Marzinotto, Gabriel  and
      Auguste, Jeremy  and
      Bechet, Frederic  and
      Damnati, Geraldine  and
      Nasr, Alexis",
    booktitle = "Proceedings of the Eleventh International Conference on Language Resources and Evaluation ({LREC} 2018)",
    month = may,
    year = "2018",
    address = "Miyazaki, Japan",
    publisher = "European Language Resources Association (ELRA)",
    url = "https://www.aclweb.org/anthology/L18-1159",
}
```

### Bibliography

* Semantic Frame Parsing for Information Extraction : the CALOR corpus (https://www.aclweb.org/anthology/L18-1159/)
* Calor-frame: un corpus de textes encyclopédiques annoté en cadres sémantiques (https://hal.archives-ouvertes.fr/hal-01780348/)
* Analyse automatique FrameNet: une étude sur un corpus français de textes encyclopédiques (https://hal.archives-ouvertes.fr/hal-01959260/)
* Adapting a FrameNet Semantic Parser for Spoken Language Understanding Using Adversarial Learning (https://arxiv.org/abs/1910.02734)
* Robust Semantic Parsing with Adversarial Learning for Domain Generalization (https://www.aclweb.org/anthology/N19-2021/)
* Sources of Complexity in Semantic Frame Parsing for Information Extraction (https://hal.archives-ouvertes.fr/hal-01731385/document)
